# Markdown Testing Grounds

## Marking Keys

### With `<kbd>`
- Use <kbd>Ctrl</kbd> + <kbd>U</kbd>

### With Backticks
- Use `Ctrl` + `U`

## Highlighting Text with `<mark>`

This is a sentence with <mark>a part of it</mark> highlighted.

## Highlighting Diffs

### With a Code Block

```diff
- removed
+ added
```

### Inline with `<del>` and `<ins>`

<del>Removed</del> <ins>added</ins>

### Render Symbols in HTML Elements

| Keys                                           | Action |
|------------------------------------------------|--------|
| <kbd>Prefix</kbd> + <kbd>&vert;</kbd>          | test   |
| <kbd>^</kbd> + <kbd>⌥</kbd> + <kbd>Space</kbd> | test   |
| <kbd>\<leader></kbd> + <kbd>bd</kbd>           | test   |
